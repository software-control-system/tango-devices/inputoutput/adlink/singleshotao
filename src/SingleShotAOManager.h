//=============================================================================
// SingleShotAOManager.h
//=============================================================================
// abstraction.......SingleShotAOManager
// class.............SingleShotAOManager
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _SINGLESHOTAO_MANAGER_H
#define _SINGLESHOTAO_MANAGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/DeviceTask.h>


#include "SingleShotAOTypesAndConsts.h"
#include <asl/SingleShotAO.h>
/*Use YAT library*/
#include <yat/memory/DataBuffer.h>
#include <yat4tango/PropertyHelper.h>
#include <map>


namespace SingleShotAO_ns
{

// ============================================================================
// class: SingleShotAOManager
// ============================================================================
class SingleShotAOManager :  public yat4tango::DeviceTask
{

public:

  //- constructor
  SingleShotAOManager (Tango::DeviceImpl * hostDevice);

  //- destructor
  ~SingleShotAOManager ();

  //- gets manager state
  Tango::DevState get_state();

  //- gets manager status
  std::string get_status ();

  //- init
  void init(asl::SingleShotAO * p_ssao, unsigned short p_nb_chan, double p_frequency);

  //- get current channel value
  double get_channel(ChannelId_t p_chIdx);
	  
  //- write channel
  void write_channel(ChannelId_t p_chIdx, double p_val);

  //- change period
  void write_frequency(double p_frequency);

  //- get inital
  Intial_t get_initial(ChannelId_t p_chIdx);

  //- set inital
  void set_initial(ChannelId_t p_chIdx, Intial_t p_initial);

  //- get speed
  Speed_t get_speed(ChannelId_t p_chIdx);

  //- set speed
  void set_speed(ChannelId_t p_chIdx, Intial_t p_speed);

  //- abort
  void abort();

protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg);

private:
  
  //- state
  Tango::DevState m_state;

  //- status
  std::string m_status;
  
  //- periodic job
  void periodic_job_i ();
  
  //- the underlying single shot AO object
  asl::SingleShotAO * m_ssao;

  //- frequency
  double m_frequency;
  
  //- initial buffer for all channels
  std::map<ChannelId_t, Intial_t> m_initials;

  //- channel buffer for all channels
  std::map<ChannelId_t, Channel_t> m_channels;

  //- speed buffer for all channels
  std::map<ChannelId_t, Speed_t> m_speeds;

  //- isRunning buffer for all channels
  std::map<ChannelId_t, bool> m_isRunning;

  //- index buffer for all ramps
  std::map<ChannelId_t, unsigned int> m_currentIndex;

  //- ramps buffer for all channels
  std::map<ChannelId_t, yat::Buffer<double>> m_ramps;

  //- channel number
  unsigned short m_nb_chan;
};

} // namespace SingleShotAO_ns

#endif // _SIGNLESHOTAO_MANAGER_H
