static const char *RcsId = "$Id $";
//+=============================================================================
//
// file :         SingleShotAOStateMachine.cpp
//
// description :  C++ source for the SingleShotAO and its alowed 
//                methods for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: pascal_verdier $
//
// $Revision: 13293 $
// $Date: 2009-04-07 12:53:56 +0200 (Tue, 07 Apr 2009) $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source$
// $Log$
// Revision 3.3  2007/10/23 14:04:30  pascal_verdier
// Spelling mistakes correction
//
// Revision 3.2  2005/03/02 14:06:15  pascal_verdier
// namespace is different than class name.
//
// Revision 3.1  2004/09/06 09:27:05  pascal_verdier
// Modified for Tango 5 compatibility.
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <tango.h>
#include <SingleShotAO.h>
#include <SingleShotAOClass.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace SingleShotAO_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		SingleShotAO::is_frequency_allowed
// 
// description : 	Read/Write allowed for frequency attribute.
//
//-----------------------------------------------------------------------------
bool SingleShotAO::is_frequency_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}//+----------------------------------------------------------------------------
//
// method : 		SingleShotAO::is_Abort_allowed
// 
// description : 	Execution allowed for Abort command.
//
//-----------------------------------------------------------------------------
bool SingleShotAO::is_Abort_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

}	// namespace SingleShotAO_ns
