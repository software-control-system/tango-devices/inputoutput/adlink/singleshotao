//=============================================================================
// SingleShotAOTypesAndConsts.h
//=============================================================================
// abstraction.......SingleShotAOTypesAndConsts
// class.............SingleShotAOTypesAndConsts
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _SINGLESHOTAO_TYPES_AND_CONSTS_H
#define _SINGLESHOTAO_TYPES_AND_CONSTS_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/DeviceTask.h>

#include <asl/DAQException.h>
/*Use YAT library*/
#include <yat/any/Any.h>

namespace SingleShotAO_ns
{

  //- board types
  #define k6208_BOARD_TYPE (const char*)"MAO_6208"
  #define k6216_BOARD_TYPE (const char*)"MAO_6216"
  #define kDEFAULT_BOARD_TYPE k6208_BOARD_TYPE

  //- names for dynamic attributes
  #define kCHANNEL "channel"
  #define kSPEED "speed"
  #define kINITIAL "initial"

  // common types
  typedef yat::uint16 ChannelId_t;
  typedef double Intial_t;
  typedef double Channel_t;
  typedef double Speed_t;


  //-----------------------------------------------------------------------------
  // method :   daq_to_tango_exception()
  // asl to tango exception conversion
  //-----------------------------------------------------------------------------
  inline Tango::DevFailed daq_to_tango_exception(const asl::DAQException& de)
  {
	Tango::DevErrorList error_list(de.errors.size());
	error_list.length(de.errors.size());

	for(unsigned int i = 0; i < de.errors.size(); i++)
	{
		error_list[i].reason = CORBA::string_dup(de.errors[i].reason.c_str());
		error_list[i].desc = CORBA::string_dup(de.errors[i].desc.c_str());
		error_list[i].origin = CORBA::string_dup(de.errors[i].origin.c_str());
		switch(de.errors[i].severity)
		{
		case asl::WARN:
			error_list[i].severity = Tango::WARN;
			break;
		case asl::PANIC:
			error_list[i].severity = Tango::PANIC;
			break;
		case asl::ERR:
		default:
			error_list[i].severity = Tango::ERR;
			break;
		}

	}
	return Tango::DevFailed(error_list);
  }
	
} // namespace SingleShotAO_ns

#endif // _SINGLESHOTAO_TYPES_AND_CONSTS_H