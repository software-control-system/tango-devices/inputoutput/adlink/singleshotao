from conan import ConanFile

class SingleShotAORecipe(ConanFile):
    name = "singleshotao"
    executable = "ds_SingleShotAO"
    version = "2.0.4"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Gwenaelle Abeille"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/inputoutput/adlink/singleshotao.git"
    description = "SingleShotAO device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("asl/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
